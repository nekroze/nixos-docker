{config, lib, pkgs, ...}:
with lib;

let
  cfg = config.modules.docker;

  empty = list: builtins.length list == 0;
  mkNonNullOpt = opt: value: optional (!isNull value) "${opt} ${value}";
  isDefaultNetwork = net: net == "host" || net == "bridge" || net == "none";
  isNotDefaultNetwork = net: !(isDefaultNetwork net);
  mkVolumeMapOpt = path: opts: "-v " + (optionalString (!isNull opts.dest) "${opts.dest}:") + path + (optionalString (!empty opts.opts) ":${concatStringsSep "," opts.opts}");
  mkVolumeMapOpts = maps: attrValues (mapAttrs mkVolumeMapOpt maps);
  mkLinkOpt = name: opts: "--link ${name}${optionalString (!isNull opts.rename) ":${opts.rename}"}";
  mkLinkOpts = links: attrValues (mapAttrs mkLinkOpt links);
  mkEnvOpt = name: value: "-e ${name}=${value}";
  mkEnvOpts = envs: attrValues (mapAttrs mkEnvOpt envs);
  mkPortOpt = host: opts: concatMapStringsSep " " (type: "-p ${opts.listenAddress}:${host}:${opts.dest}/${type}") opts.types;
  mkPortOpts = ports: attrValues (mapAttrs mkPortOpt ports);
  mkAddCapOpt = caps: optional (!empty caps) ''--cap-add ${concatStringsSep "," caps}'';
  mkDeviceOpt = dev: "--device ${dev}";
  mkDeviceOpts = devices: map mkDeviceOpt devices;

  mkDockerService = name: options: {
    name = "docker-${name}";
    value = let
      miscOpts = singleton "--name ${name}"
        ++ (optional options.privileged "--privileged")
        ++ (singleton "--log-driver=${options.logDriver}")
        ++ (mkNonNullOpt "--ip" options.ip)
        ++ (map (v: "--ulimit ${v}") options.ulimits)
        ++ (optional (!isNull options.limitMemory) "--memory ${toString options.limitMemory}M")
        ++ (optional (!isNull options.limitCpus) "--cpus ${options.limitCpus}");
      linkOpts = mkLinkOpts options.links;
      volumeOpts = mkVolumeMapOpts options.volumeMaps;
      envOpts = mkEnvOpts options.environmentVars;
      portOpts = mkPortOpts options.portMaps;
      dnsOpt = flatten (optional (options.dns != cfg.dns) (mkNonNullOpt "--dns" options.dns));
      netOpt = singleton "--network ${head options.networks}";
      capOpts = mkAddCapOpt options.additionalCapabilities;
      deviceOpts = mkDeviceOpts options.devices;
      execOpts = miscOpts ++ linkOpts ++ volumeOpts ++ envOpts ++ portOpts ++ dnsOpt ++ netOpt ++ capOpts ++ deviceOpts ++ options.extraDockerOpts;
      strContains = haystack: needle: (stringLength haystack) > (stringLength (replaceStrings [needle] [""] haystack));
      isFilePath = path: strContains path ".";
      mountDirs = map (v: v.dest) (attrValues (filterAttrs (n: v: !(isNull v.dest) && !(isStorePath v.dest) && !(isFilePath v.dest)) options.volumeMaps));
      imageName = if !isNull options.dockerfile then "nix/${name}:latest" else options.image;
      netServices = builtins.filter (n: isNotDefaultNetwork n) options.networks;
    in rec {
      requires = flatten ([
        ["network-online.target" "docker.service" ]
        (map (n: "dockernet-${n}.service") netServices)
        (map (n: "docker-${n}.service") (attrNames options.links))
        options.serviceRequires
      ]);
      after = requires;
      wantedBy = if options.autostart then ["multi-user.target"] else [];
      script = "docker start -a ${name}";
      path = [ pkgs.docker ];
      preStart = ''
        ${concatMapStringsSep "\n" (dir: "[ -e ${dir} ] || mkdir -p ${dir}") mountDirs}
        ${concatMapStringsSep "\n" (dir: "[ -d ${dir} ] && chmod 777 ${dir} || true") mountDirs}
        ${if !isNull options.dockerfile then ''
          mkdir -p /tmp/docker-nix/${name}
          cp ${options.dockerfile} /tmp/docker-nix/${name}/Dockerfile
          docker build --pull -t ${imageName} /tmp/docker-nix/${name}
          rm -rf /tmp/docker-nix/${name}
        '' else ''
          docker pull ${imageName}
        ''}
        docker stop ${name} || docker kill ${name} || true
        docker rm --volumes ${name} || true
        docker create ${concatStringsSep " " execOpts} ${imageName}${optionalString (!isNull options.command) " ${options.command}"}
        ${concatMapStringsSep "\n" (network: "docker network connect ${network} ${name}") (builtins.tail options.networks)}
      '';
      serviceConfig = rec {
        Restart = "always";
        TimeoutStartSec = "0";
        RestartSec = "3";
        ExecStop = "-${pkgs.docker}/bin/docker stop ${name}";
      };
    };
  };

  mkDockerServices = containers: mapAttrs' mkDockerService containers;
  mkDockerRestartService = name: options: {
    name = "restart-docker-${name}";
    value = {
      startAt = "daily";
      serviceConfig = {
        Type = "oneshot";
        ExecStart = "${pkgs.libudev}/bin/systemctl restart docker-${name}.service";
      };
    };
  };
  mkDockerRestartServices = containers: mapAttrs' mkDockerRestartService containers;
  mkDockerNetworkService = name: nameValuePair "dockernet-${name}" {
    requires = singleton "docker.service";
    after = singleton "docker.service";
    wantedBy = singleton "multi-user.target";
    script = "${pkgs.docker}/bin/docker network inspect ${name} 2>&1 >/dev/null || ${pkgs.docker}/bin/docker network create ${name}";
    serviceConfig = {
      Type = "oneshot";
      ExecStop = "${pkgs.docker}/bin/docker network rm ${name}";
      RemainAfterExit = "yes";
    };
  };
  collectNetworks = containers: subtractLists ["host" "bridge" "none" "lan"] (unique (flatten (map (v: v.networks) (attrValues containers))));
  mkDockerNetworkServices = containers: listToAttrs (map mkDockerNetworkService (collectNetworks containers));
in {

  options = {
    modules.docker = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = "Enable the docker host system.";
      };
      collectGarbage = mkOption {
        type = types.bool;
        default = true;
        description = "Enable the docker garbage collector to remove old images and containers daily.";
      };
      collectGarbageInterval = mkOption {
        type = types.str;
        default = "daily";
        description = "When to run the systemd timer to activate the docker garbage collector";
      };
      dns = mkOption {
        type = types.nullOr types.str;
        default = null;
        description = "Dns server to give the docker daemon.";
      };
      containerListenAddress = mkOption {
        type = types.str;
        default = "0.0.0.0";
        description = "Default address for containers ports to listen on.";
      };
      systemdCgroups = mkOption {
        type = types.bool;
        default = true;
        description = "Make docker use systemd cgroups.";
      };

      containers = let
        containerOptions = let
          volumeOptions = { options, ... }: {
            options = {
              dest = mkOption {
                default = null;
                type = types.nullOr types.path;
                description = "Path on the host to map too.";
              };
              opts = mkOption {
                default = [];
                type = types.listOf types.str;
                description = "Docker volume options.";
              };
            };
          };
          linkOptions = { options, ... }: {
            options = {
              rename = mkOption {
                default = null;
                type = types.nullOr types.str;
                description = "Rename the linked container when referenced in this container.";
              };
            };
          };
          portOptions = { name, options, ... }: {
            options = {
              dest = mkOption {
                default = name;
                type = types.str;
                description = "Destination ports to map too in the container.";
              };
              types = mkOption {
                default = [ "tcp" ];
                type = types.listOf (types.enum [ "tcp" "udp" ]);
                description = "Types of port to map; tcp, udp.";
              };
              listenAddress = mkOption {
                default = cfg.containerListenAddress;
                type = types.str;
                description = "IP address for this port to be exposed on.";
              };
            };
          };
        in { name, ... }: {

          options = {
            enable = mkOption {
              type = types.bool;
              default = true;
              description = "Enable this container.";
            };
            privileged = mkEnableOption "privileged container.";
            dailyRestart = mkOption {
              type = types.bool;
              default = false;
              description = "Repull and restart this container daily, recommended when using the latest tag for an image.";
            };
            autostart = mkOption {
              default = true;
              type = types.bool;
              description = "Autostart this container, if false the container can only be started by a link.";
            };
            image = mkOption {
              default = null;
              type = types.nullOr types.str;
              description = "Docker image this container uses.";
            };
            ip = mkOption {
              default = null;
              type = types.nullOr types.str;
              description = "IP address to assign to this container.";
            };
            ulimits = mkOption {
              default = [];
              type = types.listOf types.str;
              description = "Ulimits to set on this container.";
            };
            dns = mkOption {
              default = null;
              type = types.nullOr types.str;
              description = "DNS server this container should use.";
            };
            command = mkOption {
              default = null;
              type = types.nullOr types.str;
              description = "Command to run in the container. If default none is provided.";
            };
            limitCpus = mkOption {
              default = null;
              type = types.nullOr types.str;
              description = "Floating point number representing the cpu core load this container is allowed to consume.";
            };
            limitMemory = mkOption {
              default = null;
              type = types.nullOr types.int;
              description = "Megabytes of memory this container is allowed to consume.";
            };
            networks = mkOption {
              type = types.listOf types.str;
              default = [name];
              description = "What docker networks should this container use, defaults to its own isolated network.";
            };
            serviceRequires = mkOption {
              default = [];
              type = types.listOf types.str;
              description = "Other systemd services or objects this service requires.";
            };
            additionalCapabilities = mkOption {
              default = [];
              type = types.listOf types.str;
              description = "Capabilities to add to this container.";
            };
            devices = mkOption {
              default = [];
              type = types.listOf types.str;
              description = "Devices this container should have access to.";
            };
            links = mkOption {
              default = {};
              type = types.attrsOf (types.submodule linkOptions);
              description = "Docker links to make with other declared containers.";
            };
            volumeMaps = mkOption {
              default = {};
              type = types.attrsOf (types.submodule volumeOptions);
              description = "Docker volume maps for this container, the name is the path in the container.";
            };
            environmentVars = mkOption {
              default = {};
              type = types.attrsOf (types.loeOf types.str);
              apply = mapAttrs (n: v: if isList v then concatStringsSep ":" v else v);
              description = ''
                Set of environment variables to set in the container.
                Values can be strings or lists of string the latter are concatenated with colons.
              '';
            };
            portMaps = mkOption {
              default = {};
              type = types.attrsOf (types.submodule portOptions);
              description = "Set of port map where names are host ports.";
            };
            dockerfile = mkOption {
              default = null;
              type = types.nullOr types.path;
              description = "Dockerfile path used to build the image this container will run. When defined image is ignored.";
            };
            logDriver = mkOption {
              default = "none";
              type = types.enum [ "journald" "none" ];
              description = "Docker log driver to use for this container, journald will cause double logging.";
            };
            extraDockerOpts = mkOption {
              default = [];
              type = types.listOf types.str;
              description = "Extra docker options to pass to this containers creation command.";
            };
          };
        };
      in mkOption {
        default = {};
        type = types.attrsOf (types.submodule containerOptions);
        description = "Attribute set of containers to run on this host.";
      };
    };
  };

  config = let
    enabledContainers = filterAttrs (n: v: v.enable) cfg.containers;
  in mkIf cfg.enable {
    assertions = let
      containers = attrNames cfg.containers;
      containersHalfHead = take ((length containers) / 2) containers;
      containersHalfTail = drop ((length containers) / 2) containers;
      mkContainerImageAssert = container: {
        assertion = !isNull cfg.containers."${container}".image || !isNull cfg.containers."${container}".dockerfile;
        message = "Container ${container} requires either an image or a dockerfile be defined.";
      };
      mkContainerPortAssert = source: target: {
        assertion = length(intersectLists (attrNames cfg.containers."${source}".portMaps) (attrNames cfg.containers."${target}".portMaps)) == 0;
        message = "Containers ${source} and ${target} have conflicting port definitions.";
      };
    in [
    ] ++ (flatten(map (s: map (t: mkContainerPortAssert s t) containersHalfHead) containersHalfTail))
    ++ (map mkContainerImageAssert containers);

    boot.kernelParams = [
      "cgroup_enable=memory"
      "swapaccount=1"
    ];
    virtualisation.docker = {
      enable = true;
      extraOptions = concatStringsSep " " [
        (optionalString cfg.systemdCgroups "--exec-opt native.cgroupdriver=systemd")
        (optionalString (!isNull cfg.dns) "--dns ${cfg.dns}")
      ];
    };

    systemd.services = mkDockerServices enabledContainers //
    mkDockerNetworkServices enabledContainers //
    mkDockerRestartServices enabledContainers //
    (if cfg.collectGarbage then {
      dockergc = rec {
        requires = ["docker.service"];
        after = requires;
        startAt = cfg.collectGarbageInterval;
        serviceConfig = {
          Type = "oneshot";
          ExecStart = "${pkgs.docker-gc}/bin/docker-gc";
        };
      };
    } else {}) //
    {
      docker = rec {
        requires = ["network-online.target"];
        after = requires;
      };
    };
  };
}
