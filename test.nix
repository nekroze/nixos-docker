import <nixpkgs/nixos/tests/make-test.nix> {
  machine = { config, pkgs, ... }: {
    imports = [
      ./default.nix
    ];
    modules.docker = {
      enable = true;
      containers = {
        myredis = {
          image = "redis:alpine";
          limitMemory = 1024;
        };
        myredisdf = {
          dockerfile = pkgs.writeText "myredis.Dockerfile" "FROM redis:alpine";
        };
      };
    };
  };

  testScript = ''
    subtest "docker is runner", sub {
      $machine->waitForUnit("docker.service");
    };
    subtest "redis container is running", sub {
      $machine->waitForUnit("docker-myredis.service");
      $machine->succeed("docker inspect --format='{{.State.Status}}' myredis | grep running");
    };
    subtest "myredis docker network is running", sub {
      $machine->waitForUnit("dockernet-myredis.service");
      $machine->succeed("docker network inspect myredis")
    };
    subtest "redis container built from dockerfile is running", sub {
      $machine->waitForUnit("docker-myredisdf.service");
      $machine->succeed("docker inspect --format='{{.State.Status}}' myredisdf | grep running");
    };
  '';
}
